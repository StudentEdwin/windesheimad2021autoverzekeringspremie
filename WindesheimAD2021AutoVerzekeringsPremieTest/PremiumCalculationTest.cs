﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using System.Threading.Tasks;

namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
    using Xunit;

    public class PremiumCalculationTest
    {
        private Mock<Vehicle> MockLamborghiniAventador(int age = 2017, int valueInEuros = 580000, int powerInKw = 700)
        {
            var lamborghiniAventador = new Mock<Vehicle>(MockBehavior.Default, 0, 0, 0);
            lamborghiniAventador.Setup(Vehicle => Vehicle.Age).Returns(age);
            lamborghiniAventador.Setup(Vehicle => Vehicle.ValueInEuros).Returns(valueInEuros);
            lamborghiniAventador.Setup(Vehicle => Vehicle.PowerInKw).Returns(powerInKw);
            return lamborghiniAventador;
        }

        [Fact]
        public void BasicPremiumCalculation()
        {
            // Arrange 
            var vehicle = new Vehicle(700, 580000, 2017);

            var expectedPremium = ((double) vehicle.ValueInEuros / 100 - vehicle.Age + (double) vehicle.PowerInKw / 5) / 3;

            // Act
            var actualValue = PremiumCalculation.CalculateBasePremium(vehicle);

            // Assert
            Assert.Equal(expectedPremium, actualValue);
        }

        [Fact]
        public void StoreForYoungerThan23()
        {
            // Arrange
            var lamborghiniAventador = MockLamborghiniAventador();

            var YoungDriver = new PolicyHolder(20, "01-01-2005", 9999, 0);
            var OldDriver = new PolicyHolder(60, "01-01-2005", 9999, 0);

            // Act
            var PremiumOldDriver = new PremiumCalculation(lamborghiniAventador.Object, OldDriver, InsuranceCoverage.WA);
            var PremiumYoungDriver = new PremiumCalculation(lamborghiniAventador.Object, YoungDriver, InsuranceCoverage.WA);


            // Assert
            Assert.Equal(PremiumYoungDriver.PremiumAmountPerYear, PremiumOldDriver.PremiumAmountPerYear * 1.15);
        }

        [Theory]
        [InlineData(5, false)]
        [InlineData(4, true)]
        public void PremiumLessThanFiveYears(int driverAge, bool elevation)
        {
            // Arrange
            var lamborghiniAventador = MockLamborghiniAventador();

            var startDateDriverLicense = DateTime.Now.AddYears(-driverAge).ToString();

            var youngDriver = new PolicyHolder(40, startDateDriverLicense, 6666, 0);
            var oldDriver = new PolicyHolder(40, "01-01-2005", 6666, 0);

            var PremiumOldDriver = new PremiumCalculation(lamborghiniAventador.Object, oldDriver, InsuranceCoverage.WA);

            var expectedValue =
                elevation ? PremiumOldDriver.PremiumAmountPerYear * 1.15 : PremiumOldDriver.PremiumAmountPerYear;

            // Act
            var PremiumYoungDriver = new PremiumCalculation(lamborghiniAventador.Object, youngDriver, InsuranceCoverage.WA);

            // Assert
            Assert.Equal(expectedValue, PremiumYoungDriver.PremiumAmountPerYear);
        }

        [Fact]
        public void StorePostcode5Procent()
        {
            // Arrange
            var lamborghiniAventador = MockLamborghiniAventador();

            var StorePostcodeFor5Procent = new PolicyHolder(40, "01-01-2005", 2000, 0);
            var StorePostcodeFor0Procent = new PolicyHolder(40, "01-01-2005", 8800, 0);

            // Act
            var Premium5Postcode = new PremiumCalculation(lamborghiniAventador.Object, StorePostcodeFor5Procent, InsuranceCoverage.WA);
            var Premium0Postcode = new PremiumCalculation(lamborghiniAventador.Object, StorePostcodeFor0Procent, InsuranceCoverage.WA);


            // Assert
            Assert.Equal(Premium5Postcode.PremiumAmountPerYear, Premium0Postcode.PremiumAmountPerYear * 1.05);
        }

        [Fact]
        public void StorePostcode2Procent()
        {
            // Arrange
            var lamborghiniAventador = MockLamborghiniAventador();

            var StorePostcodeFor2Procent = new PolicyHolder(40, "01-01-2005", 3700, 0);
            var StorePostcodeFor0Procent = new PolicyHolder(40, "01-01-2005", 8800, 0);

            // Act
            var Premium2Postcode = new PremiumCalculation(lamborghiniAventador.Object, StorePostcodeFor2Procent, InsuranceCoverage.WA);
            var Premium0Postcode = new PremiumCalculation(lamborghiniAventador.Object, StorePostcodeFor0Procent, InsuranceCoverage.WA);


            // Assert
            Assert.Equal(Premium2Postcode.PremiumAmountPerYear, Premium0Postcode.PremiumAmountPerYear * 1.02);
        }

        [Fact]
        public void WaPlus20Procent()
        {
            // Arrange
            var lamborghiniAventador = MockLamborghiniAventador();

            var policyHolder = new PolicyHolder(40, "01-01-2005", 8800, 0);

            // Act
            var waPremie = new PremiumCalculation(lamborghiniAventador.Object, policyHolder, InsuranceCoverage.WA);
            var waPlusPremie = new PremiumCalculation(lamborghiniAventador.Object, policyHolder, InsuranceCoverage.WA_PLUS);

            // Assert
            Assert.Equal(waPremie.PremiumAmountPerYear * 1.20, waPlusPremie.PremiumAmountPerYear);
        }

        [Fact]
        public void AllRiskIsDubblePremium()
        {
            // Arrange
            var lamborghiniAventador = MockLamborghiniAventador();

            var policyHolder = new PolicyHolder(40, "01-01-2005", 8800, 0);

            // Act
            var waPremie = new PremiumCalculation(lamborghiniAventador.Object, policyHolder, InsuranceCoverage.WA);
            var waPlusPremie = new PremiumCalculation(lamborghiniAventador.Object, policyHolder, InsuranceCoverage.ALL_RISK);

            // Assert
            Assert.Equal(waPremie.PremiumAmountPerYear * 2, waPlusPremie.PremiumAmountPerYear);
        }


        [Theory]
        [InlineData(5,0)]
        [InlineData(6, 5)]
        [InlineData(7, 10)]
        [InlineData(17, 60)]
        [InlineData(18, 65)]
        [InlineData(19, 65)]
        public void DamageFreeYears(int noClaimYears, double discountPercentage)
        {
            // Arrange
            var lamborghiniAventador = MockLamborghiniAventador();

            var damageFreeYearsNull = new PolicyHolder(33, "01-01-2005", 8800, 0);
            var damageFreeYearsSix = new PolicyHolder(33, "01-01-2005", 8800, noClaimYears);

            // Act
            var year5 = new PremiumCalculation(lamborghiniAventador.Object, damageFreeYearsNull, InsuranceCoverage.WA);
            var year6 = new PremiumCalculation(lamborghiniAventador.Object, damageFreeYearsSix, InsuranceCoverage.WA);


            // Assert
            Assert.Equal(year6.PremiumAmountPerYear, year5.PremiumAmountPerYear * (1- discountPercentage / 100));
        }

        [Fact]
        public void PremiumPerMonth()
        {
            // Arrange
            var lamborghiniAventador = MockLamborghiniAventador();

            var damageFreeYearsNull = new PolicyHolder(33, "01-01-2005", 8800, 0);

            var premiumCalculator = new PremiumCalculation(lamborghiniAventador.Object, damageFreeYearsNull, InsuranceCoverage.WA);

            var expected = Math.Round(premiumCalculator.PremiumAmountPerYear / 12 , 2 );

            // Act
            var actualValue = premiumCalculator.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.MONTH);

            // Assert
            Assert.Equal(expected, actualValue);
        }

        [Fact]
        public void PremiumPerYear()
        {
            // Arrange
            var lamborghiniAventador = MockLamborghiniAventador();

            var damageFreeYearsNull = new PolicyHolder(33, "01-01-2005", 8800, 0);

            var premiumCalculator = new PremiumCalculation(lamborghiniAventador.Object, damageFreeYearsNull, InsuranceCoverage.WA);

            var expected = Math.Round(premiumCalculator.PremiumAmountPerYear * (1 - 0.025), 2);

            // Act
            var actualValue = premiumCalculator.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR);

            // Assert
            Assert.Equal(expected, actualValue);
        }
    }
}
