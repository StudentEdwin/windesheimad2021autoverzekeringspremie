```

                  _____          _____   _____ _    _ _____  ______     _    _ _   _ _____ _______ _______ ______  _____ _______ 
                 / ____|   /\   |  __ \ / ____| |  | |  __ \|  ____|   | |  | | \ | |_   _|__   __|__   __|  ____|/ ____|__   __|
                | |       /  \  | |__) | (___ | |  | | |__) | |__      | |  | |  \| | | |    | |     | |  | |__  | (___    | |   
                | |      / /\ \ |  _  / \___ \| |  | |  _  /|  __|     | |  | | . ` | | |    | |     | |  |  __|  \___ \   | |   
                | |____ / ____ \| | \ \ ____) | |__| | | \ \| |____    | |__| | |\  |_| |_   | |     | |  | |____ ____) |  | |   
                 \_____/_/    \_\_|  \_\_____/ \____/|_|  \_\______|    \____/|_| \_|_____|  |_|     |_|  |______|_____/   |_|   
                                                                                                                
                                                                                                                
```


### Deel 1:

- Het doel van de unittest
- Waarom heb je voor deze specifieke (data) invulling van de unittest hebt gekozen
- Welke technieken je gebruikt hebt om tot deze data te komen

<br/>

### Deel 1 uitleg:

10 unittesten
- 1 BasicPremiumCalculation
- 2 StoreForYoungerThan23
- 3 PremiumLessThanFiveYears
- 4 StorePostcode5Procent
- 5 StorePostcode2Procent
- 6 WaPlus20Procent
- 7 AllRiskIsDubblePremium
- 8 DamageFreeYears
- 9 PremiumPerMonth
- 10 PremiumPerYear

<br/>

### BasicPremiumCalculation uitleg:
Het doel van deze unittest is om te kijken of de basis premie correct wordt berekent. <br/>
Voor de data leek het mij leuk om een Lamborghini Aventador te kiesen, hier heb ik de waardes van op gezocht.
<br/> De techniek die ik gebruikt heb is [Fact]

<br/>

### StoreForYoungerThan23 uitleg:
Het doel van deze unittest is om te kijken of de persoon ouder of jonger is dan 23. <br/>
Zelfde vehicle data als over al gebruikt en voor de leeftijd heb ik gekozen voor 20 en 60. 
<br/> De techniek die ik gebruikt heb is [Fact].

<br/>

### PremiumLessThanFiveYears uitleg:
Het doel van deze unittest is om te kijken of de persoon zijn/haar rijbewijs korter heeft dan 5 jaar. <br/>
Zelfde vehicle data als over al gebruikt en voor de 'driverAge', 5 jaar en 4 jaar. 
<br/> De techniek die ik gebruikt heb is [Theory].

<br/>

### StorePostcode5Procent uitleg:
Het doel van deze unittest is om te kijken of de persoon 5% risco opslag heeft of niet.<br/>
Hiervoor moeten we een postcode geven die buiten en binnen de 5% past.
<br/> De techniek die ik gebruikt heb is [Fact].

<br/>

### StorePostcode2Procent uitleg:
Het doel van deze unittest is om te kijken of de persoon 2% risco opslag heeft of niet.<br/>
Hiervoor moeten we een postcode geven die buiten en binnen de 2% past.
<br/> De techniek die ik gebruikt heb is [Fact].

<br/>

### WaPlus20Procent uitleg:
Het doel van deze unittest is om te kijken of de InsuranceCoverage.Wa_PLUS 20% duurder is dan de WA.<br/>
Hier geven we standaard informatie mee en vergelijken de WA met WA_PLUS.
<br/> De techniek die ik gebruikt heb is [Fact].

<br/>

### AllRiskIsDubblePremium uitleg:
Het doel van deze unittest is om te kijken of de All_RISK daadwerkelijk het dubblen is van de WA.<br/>
Hierbij vergelijken we de WA met ALL_RISK en geven dus de zelfde informatie mee alleen met een andere InsuranceCoverage.
<br/> De techniek die ik gebruikt heb is [Fact].

<br/>

### DamageFreeYears uitleg:
Het doel van deze unittest is om te kijken of de DamageFreeYears goed doorlopen tot 65% en daarna daar op blijft.<br/>
De data die ik in de Theory hebben gezet is data die test de 5, 6, 7, 17, 18 en 19 jaar. Met het discountPercentage die erbij zou moeten horen.
<br/> De techniek die ik gebruikt heb is [Theory].

<br/>

### PremiumPerMonth uitleg:
Het doel van deze unittest is om te kijken of premie per maand klopt.<br/>
Hierbij zeg ik standaart informatie zonder toegevoegde korting of iets, dan rekenen ik het uit in 1 jaar en deel ik het door 12. <br/>Vervolgens rond ik het af op twee cijfers achter de comma.
<br/> De techniek die ik gebruikt heb is [Fact].

<br/>

### PremiumPerYear uitleg:
Het doel van deze unittest is om te kijken of je naar een jaar betaling ook echt de 2,5% korting krijgt.<br/>
De data is weer standaart om een zo goed mogelijke test te krijgen.
<br/> De techniek die ik gebruikt heb is [Fact].

<br/>

### Deel 2:
- Wat heb je getest?
- Wat had je verwacht?
- Waar komt deze verwachting vandaan?

<br/>

### Deel 2 uitleg:

De fouten die ik heb gevonden zijn:
- CalculateBasePremium
```
 if(policyHolder.Age < 23 || policyHolder.LicenseAge <= 5)
            {
                premium *= 1.15;
            }
```
- Naar
```
 if(policyHolder.Age < 23 || policyHolder.LicenseAge < 5)
            {
                premium *= 1.15;
            }
```
### Uitleg: 
Gestest of de Age groter is dan 23 of LicenseAge kleiner is dan 5.<br/>
Ik had verwacht dat die zou falen omdat je test voor kleiner dan 5 en niet kleiner of het zelfde.<br/>
Omdat de eind_opdracht niet goed was.

<br/>

<br/>

- UpdatePremiumForNoClaimYears
```
 private static double UpdatePremiumForNoClaimYears(double premium, int years)
        {
            int NoClaimPercentage = (years - 5) * 5;
            if (NoClaimPercentage > 65) { NoClaimPercentage = 65; }
            if (NoClaimPercentage < 0) { NoClaimPercentage = 0; }
            return premium * ((100 - NoClaimPercentage) / 100);
        }
```
- Naar
```
private static double UpdatePremiumForNoClaimYears(double premium, int years)
        {
            int NoClaimPercentage = (years - 5) * 5;
            if (NoClaimPercentage > 65) { NoClaimPercentage = 65; }
            if (NoClaimPercentage < 0) { NoClaimPercentage = 0; }
            return premium * ((double)(100 - NoClaimPercentage) / 100);
        }
```
### Uitleg: 
Hier moest een Double worden toegevoegd, als je dit niet doet wordt het een int en klopt de test niet meer.<br/>
Ik had verwacht dat het zou werken maar ik had niet door dat het een int was in plaats van een double.<br/>
Komt omdat ik niet goed aan het kijken was.

<br/>

<br/>

- PremiumPaymentAmount
```
  internal double PremiumPaymentAmount(PaymentPeriod period)
        {
            double result = period == PaymentPeriod.YEAR ? PremiumAmountPerYear / 1.025 : PremiumAmountPerYear / 12;
            return Math.Round(result, PRECISION);
        }
```
- Naar
```
  internal double PremiumPaymentAmount(PaymentPeriod period)
        {
            double result = period == PaymentPeriod.YEAR ? PremiumAmountPerYear * (1 - 0.025) : PremiumAmountPerYear / 12;
            return Math.Round(result, PRECISION);
        }
```

### Uitleg: 
Hier moest ik testen of premie per jaar goed was. Zat een fouthe in de code waardoor je het niet kon uitrekenen.<br/>
Ik had eerst niet grezien dat er een fout in stond en dacht dat het gewoon zou werken.<br/>
<br/>

<br/>

- CalculateBasePremium
```
internal static double CalculateBasePremium(Vehicle vehicle)
        {
            return vehicle.ValueInEuros / 100 - vehicle.Age + vehicle.PowerInKw / 5 / 3;
        }
```
- Naar
```
internal static double CalculateBasePremium(Vehicle vehicle)
        {
            return ((double)vehicle.ValueInEuros / 100 - vehicle.Age + (double)vehicle.PowerInKw / 5) / 3;
        }
```

### Uitleg: 
Het premie bedrag moest worden afgerond naar 2 cijfers achter de komma, maar de ValueInEuros en PowerInKw waren een int.<br/>
Hier moest een double van worden gemaakt anders werkt het niet.